## 将第三方库(.a 或 .framework)打包进自己的customFramework中

- 1. 创建xx.framework工程

     ![image_one](./images/image_one.png)

- 2. 更改相关Build Setting

     - 修改最低支持系统版本..

       ![image_one](./images/image_two.png)

     - 修改编译打包输出条件 (关闭只对当前连接设备所支持的指令集编译), 增加i386架构有效指令集(iPhone 5及以下模拟器架构)

       ![image_one](./images/image_three.png)

     - 修改编译打包输出库类型:Static Library

       ![image_one](./images/image_four.png)

     - 开启bitCode,增加编译打包时正式输出支持bitCode库条件 -fembed-bitcode

       ![image_one](./images/image_five.png)

     - 开启安装包优化:DEPLOYMENT_POSTPROCESSING=YES;编译优化STRIP_STYLE=Debugging Symbols设置只在debug时候剥离所有的符号

       ![image_one](./images/image_six.png)

     - 将第三方framework拖入到项目中,先勾选Add  to Targets

       ![image_one](./images/image_seven.png)

     - 前往Taget-BuildPhasse-Link Binary With Libraries,将刚才拖入到项目中的XXX.framework删掉

       然后将XXX.framework中XXX文件分别+号添加到Link Binary With Libraies和Compile Source中参与编译(如果第三方是.a,只需将.a添加到Compile Source中),Compile Source中文件是所有参与编译打包的.m文件或二进制文件

       ![image_one](./images/image_eight.png)

       - 选定模拟器+Debug/Release 和 真机+Debug/Release 分别编译出真机/模拟器(开发或发布版).framework

         ![image_one](./images/image_nine.png)

         ![image_one](./images/image_ten.png)

       - 最后创建一个RunScript Target 来合并已经打包好的相关.framework

         ![image_one](./images/image_eleven.png)

         ![image_one](./images/image_twelve.png)

       - 选中创建Run Script Target来添加script代码

         ![image_one](./images/image_thirteen.png)		

         - sehll脚本

           > \# Type a script or drag a script file from your workspace to insert its path.
           >
           > \# Sets the target folders and the final framework product.
           >
           >
           >
           > \# 如果工程名称和Framework的Target名称不一样的话，要自定义FMKNAME
           >
           >
           >
           > \# 例如: FMK_NAME ="MyFramework"
           >
           >
           >
           > FMK_NAME=${PROJECT_NAME}
           >
           > \#FMK_NAME="ABCtimeReadingBookSDK"
           >
           >
           >
           > \# Install dir will be the final output to the framework.
           >
           >
           >
           > \# The following line create it in the root folder of the current project.
           >
           >
           >
           > INSTALL_DIR=~/desktop/${FMK_NAME}.framework
           >
           >
           >
           > \# Working dir will be deleted after the framework creation.
           >
           >
           >
           > WRK_DIR=build
           >
           >
           >
           > \# 真机路径
           >
           > DEVICE_DIR=${BUILD_DIR}/Debug-iphoneos/${FMK_NAME}.framework
           >
           >
           >
           > \# 模拟器路径
           >
           > SIMULATOR_DIR=${BUILD_DIR}/Debug-iphonesimulator/${FMK_NAME}.framework
           >
           >
           >
           > \# -configuration ${CONFIGURATION}
           >
           >
           >
           > \# Clean and Building both architectures.
           >
           >
           >
           > xcodebuild -configuration "Debug" -target"${FMK_NAME}" -sdk iphoneos clean build
           >
           >
           >
           > xcodebuild -configuration "Debug" -target"${FMK_NAME}" -sdk iphonesimulator clean build
           >
           >
           >
           > \# Cleaning the oldest.
           >
           >
           >
           > if [ -d "${INSTALL_DIR}" ]
           >
           >
           >
           > then
           >
           >
           >
           > rm -rf "${INSTALL_DIR}"
           >
           >
           >
           > fi
           >
           >
           >
           > mkdir -p "${INSTALL_DIR}"
           >
           >
           >
           > cp -R "${DEVICE_DIR}/" "${INSTALL_DIR}/"
           >
           >
           >
           > \# Uses the Lipo Tool to merge both binary files (i386 + armv6/armv7) into one Universal final product.
           >
           >
           >
           > lipo -create "${DEVICE_DIR}/${FMK_NAME}" "${SIMULATOR_DIR}/${FMK_NAME}" -output "${INSTALL_DIR}/${FMK_NAME}"
           >
           >
           >
           > rm -r "${WRK_DIR}"
           >
           >
           >
           > open "${INSTALL_DIR}"

       - 最后执行shell脚本,合并模拟器和真机framework

         ![image_one](./images/image_fourteen.png)

